/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author robbez
 */
@Stateless
public class MovieFacade extends AbstractFacade<Movie> implements MovieFacadeLocal {

    @PersistenceContext(unitName = "GarbageAndGoods-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MovieFacade() {
        super(Movie.class);
    }
    
    @Override
    public void purchaseMovie(String movieId) {
        Movie retrievedMovie = this.find(movieId); 
        if(retrievedMovie != null)
        {
            retrievedMovie.setInventory(retrievedMovie.getInventory() - 1);
            edit(retrievedMovie);
        }
    }
}
