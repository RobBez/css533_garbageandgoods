/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejb;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author robbez
 */
@Entity
@Table(name = "card", catalog = "garbageandgoods", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Card.findAll", query = "SELECT c FROM Card c"),
    @NamedQuery(name = "Card.findByCardID", query = "SELECT c FROM Card c WHERE c.cardID = :cardID"),
    @NamedQuery(name = "Card.findByCardHolder", query = "SELECT c FROM Card c WHERE c.cardHolder = :cardHolder"),
    @NamedQuery(name = "Card.findByCardExpire", query = "SELECT c FROM Card c WHERE c.cardExpire = :cardExpire"),
    @NamedQuery(name = "Card.findByCardCVS", query = "SELECT c FROM Card c WHERE c.cardCVS = :cardCVS")})
public class Card implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "cardID", nullable = false, length = 16)
    private String cardID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "cardHolder", nullable = false, length = 50)
    private String cardHolder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cardExpire", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date cardExpire;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "cardCVS", nullable = false, length = 3)
    private String cardCVS;
    @ManyToMany(mappedBy = "cardCollection")
    private Collection<Customer> customerCollection;
    @OneToMany(mappedBy = "cardID")
    private Collection<Purchase> purchaseCollection;

    public Card() {
    }

    public Card(String cardID) {
        this.cardID = cardID;
    }

    public Card(String cardID, String cardHolder, Date cardExpire, String cardCVS) {
        this.cardID = cardID;
        this.cardHolder = cardHolder;
        this.cardExpire = cardExpire;
        this.cardCVS = cardCVS;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public Date getCardExpire() {
        return cardExpire;
    }

    public void setCardExpire(Date cardExpire) {
        this.cardExpire = cardExpire;
    }

    public String getCardCVS() {
        return cardCVS;
    }

    public void setCardCVS(String cardCVS) {
        this.cardCVS = cardCVS;
    }

    @XmlTransient
    public Collection<Customer> getCustomerCollection() {
        return customerCollection;
    }

    public void setCustomerCollection(Collection<Customer> customerCollection) {
        this.customerCollection = customerCollection;
    }

    @XmlTransient
    public Collection<Purchase> getPurchaseCollection() {
        return purchaseCollection;
    }

    public void setPurchaseCollection(Collection<Purchase> purchaseCollection) {
        this.purchaseCollection = purchaseCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardID != null ? cardID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Card)) {
            return false;
        }
        Card other = (Card) object;
        if ((this.cardID == null && other.cardID != null) || (this.cardID != null && !this.cardID.equals(other.cardID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ejb.Card[ cardID=" + cardID + " ]";
    }
    
}
