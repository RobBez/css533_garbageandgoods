/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejb;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author robbez
 */
@Entity
@Table(name = "Purchase", catalog = "garbageandgoods", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Purchase.findAll", query = "SELECT p FROM Purchase p"),
    @NamedQuery(name = "Purchase.findByPurchaseID", query = "SELECT p FROM Purchase p WHERE p.purchaseID = :purchaseID"),
    @NamedQuery(name = "Purchase.findByPurchaseDate", query = "SELECT p FROM Purchase p WHERE p.purchaseDate = :purchaseDate"),
    @NamedQuery(name = "Purchase.findByTotalPrice", query = "SELECT p FROM Purchase p WHERE p.totalPrice = :totalPrice")})
public class Purchase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "purchaseID", nullable = false)
    private Integer purchaseID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "purchaseDate", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date purchaseDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "totalPrice", precision = 10, scale = 2)
    private Double totalPrice;
    @JoinColumn(name = "movieID", referencedColumnName = "movieID")
    @ManyToOne
    private Movie movieID;
    @JoinColumn(name = "customerID", referencedColumnName = "customerID")
    @ManyToOne
    private Customer customerID;
    @JoinColumn(name = "cardID", referencedColumnName = "cardID")
    @ManyToOne
    private Card cardID;

    public Purchase() {
    }

    public Purchase(Integer purchaseID) {
        this.purchaseID = purchaseID;
    }

    public Purchase(Integer purchaseID, Date purchaseDate) {
        this.purchaseID = purchaseID;
        this.purchaseDate = purchaseDate;
    }

    public Integer getPurchaseID() {
        return purchaseID;
    }

    public void setPurchaseID(Integer purchaseID) {
        this.purchaseID = purchaseID;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Movie getMovieID() {
        return movieID;
    }

    public void setMovieID(Movie movieID) {
        this.movieID = movieID;
    }

    public Customer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customer customerID) {
        this.customerID = customerID;
    }

    public Card getCardID() {
        return cardID;
    }

    public void setCardID(Card cardID) {
        this.cardID = cardID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (purchaseID != null ? purchaseID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Purchase)) {
            return false;
        }
        Purchase other = (Purchase) object;
        if ((this.purchaseID == null && other.purchaseID != null) || (this.purchaseID != null && !this.purchaseID.equals(other.purchaseID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ejb.Purchase[ purchaseID=" + purchaseID + " ]";
    }
    
}
