/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ejb;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author robbez
 */
@Entity
@Table(name = "movie", catalog = "garbageandgoods", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Movie.findAll", query = "SELECT m FROM Movie m"),
    @NamedQuery(name = "Movie.findByMovieID", query = "SELECT m FROM Movie m WHERE m.movieID = :movieID"),
    @NamedQuery(name = "Movie.findByTitle", query = "SELECT m FROM Movie m WHERE m.title = :title"),
    @NamedQuery(name = "Movie.findByReleaseDate", query = "SELECT m FROM Movie m WHERE m.releaseDate = :releaseDate"),
    @NamedQuery(name = "Movie.findByPurchasePrice", query = "SELECT m FROM Movie m WHERE m.purchasePrice = :purchasePrice"),
    @NamedQuery(name = "Movie.findByDuration", query = "SELECT m FROM Movie m WHERE m.duration = :duration"),
    @NamedQuery(name = "Movie.findBySynopsis", query = "SELECT m FROM Movie m WHERE m.synopsis = :synopsis"),
    @NamedQuery(name = "Movie.findByCoverImage", query = "SELECT m FROM Movie m WHERE m.coverImage = :coverImage"),
    @NamedQuery(name = "Movie.findByReviewRating", query = "SELECT m FROM Movie m WHERE m.reviewRating = :reviewRating"),
    @NamedQuery(name = "Movie.findByMovieLink", query = "SELECT m FROM Movie m WHERE m.movieLink = :movieLink"),
    @NamedQuery(name = "Movie.findByInventory", query = "SELECT m FROM Movie m WHERE m.inventory = :inventory")})
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "movieID", nullable = false, length = 50)
    private String movieID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "title", nullable = false, length = 50)
    private String title;
    @Column(name = "releaseDate")
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "purchasePrice", precision = 10, scale = 2)
    private Double purchasePrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "duration", nullable = false)
    @Temporal(TemporalType.TIME)
    private Date duration;
    @Size(max = 500)
    @Column(name = "synopsis", length = 500)
    private String synopsis;
    @Size(max = 100)
    @Column(name = "coverImage", length = 100)
    private String coverImage;
    @Column(name = "reviewRating", precision = 2, scale = 2)
    private Double reviewRating;
    @Size(max = 500)
    @Column(name = "movieLink", length = 500)
    private String movieLink;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inventory", nullable = false)
    private int inventory;
    @OneToMany(mappedBy = "movieID")
    private Collection<Purchase> purchaseCollection;

    public Movie() {
    }

    public Movie(String movieID) {
        this.movieID = movieID;
    }

    public Movie(String movieID, String title, Date duration, int inventory) {
        this.movieID = movieID;
        this.title = title;
        this.duration = duration;
        this.inventory = inventory;
    }

    public String getMovieID() {
        return movieID;
    }

    public void setMovieID(String movieID) {
        this.movieID = movieID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Date getDuration() {
        return duration;
    }

    public void setDuration(Date duration) {
        this.duration = duration;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Double getReviewRating() {
        return reviewRating;
    }

    public void setReviewRating(Double reviewRating) {
        this.reviewRating = reviewRating;
    }

    public String getMovieLink() {
        return movieLink;
    }

    public void setMovieLink(String movieLink) {
        this.movieLink = movieLink;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    @XmlTransient
    public Collection<Purchase> getPurchaseCollection() {
        return purchaseCollection;
    }

    public void setPurchaseCollection(Collection<Purchase> purchaseCollection) {
        this.purchaseCollection = purchaseCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (movieID != null ? movieID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movie)) {
            return false;
        }
        Movie other = (Movie) object;
        if ((this.movieID == null && other.movieID != null) || (this.movieID != null && !this.movieID.equals(other.movieID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ejb.Movie[ movieID=" + movieID + " ]";
    }
    
}
