/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.HelperServlets;

import Common.AppwideConstants;
import com.ejb.Card;
import com.ejb.CardFacadeLocal;
import com.ejb.Customer;
import com.ejb.CustomerFacadeLocal;
import com.ejb.Movie;
import com.ejb.MovieFacadeLocal;
import com.ejb.Purchase;
import com.ejb.PurchaseFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author robbez
 */
public class PurchaseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @EJB
    private MovieFacadeLocal mf;
    
    @EJB
    private PurchaseFacadeLocal pf;
    
    @EJB
    private CustomerFacadeLocal custf;
    
    @EJB
    private CardFacadeLocal cardf;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        boolean signedIn = (String) request.getSession().getAttribute("userid") != null;
        if(signedIn)
        {
            String movieID = request.getParameter("movieID");
            String userID = (String) request.getSession().getAttribute("userid");
            String cardID = getServletContext().getInitParameter(AppwideConstants.COMMON_CARD_KEY);
            
            Movie retrievedMovie = mf.find(movieID); 
            Customer retrievedCustomer = custf.find(userID);
            Card commonCard = cardf.find(cardID);

            if(retrievedMovie != null && retrievedCustomer != null && commonCard != null)
            {
               mf.purchaseMovie(movieID);
               Purchase newPurchase = new Purchase();
             
               newPurchase.setCardID(commonCard);
               newPurchase.setCustomerID(retrievedCustomer);
               newPurchase.setMovieID(retrievedMovie);
               newPurchase.setTotalPrice(retrievedMovie.getPurchasePrice());
               
               java.util.Calendar cal = java.util.Calendar.getInstance();
               java.util.Date utilDate = cal.getTime();
               Date sqlDate = new Date(utilDate.getTime());
               
               newPurchase.setPurchaseDate(sqlDate);
               pf.create(newPurchase);
            

                try (PrintWriter out = response.getWriter()) {
                    /* TODO output your page here. You may use following sample code. */
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet PurchaseServlet</title>");            
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>Congratulations, you have successfully purchased " + retrievedMovie.getTitle() + "!  <a href='index.jsp'>Go Home</a></h1>");
                    out.println("</body>");
                    out.println("</html>");
                }
            
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
