/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.HelperServlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author robbez
 */

@WebServlet(name = "CheckLoginServlet")
public class CheckLoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String loggedDropDown = "";
            String userid = (String) request.getSession().getAttribute("userid");
            boolean signedIn = false;
            
            	if (userid != null) {
                    signedIn = true;

                    loggedDropDown = " <ul class=\"nav navbar-nav navbar-right\" style=\"margin-right: 20px\"><li class=\"dropdown\" id=\"accountDropdown\"><a href=\"#\""
                                    + "class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\""
                                    + "aria-expanded=\"false\">Dropdown <span class=\"caret\"></span>"
                                    + "</a>"
                                    + "<ul class=\"dropdown-menu\" role=\"menu\">"
                                    + "<li class=\"divider\"></li>"
                                    + "<li><a href=\"LogoutServlet\">Logout</a></li>"
                                    + "</ul></li></ul>";

                } else {
                        signedIn = false;
                        loggedDropDown = "<ul class=\"nav navbar-nav navbar-right\" style=\"margin-right: 20px\"><form method=\"post\" action=\"LoginServlet\""
                                        + "class=\"navbar-form navbar-nav navbar-right\" name=\"loginForm\""
                                        + "id=\"loginForm\">"
                                        + "<div class=\"form-group\">"
                                        + "<span class=\"glyphicon glyphicon-remove-sign\" aria-hidden=\"true\"></span>\""
                                        + "<input type=\"text\" placeholder=\"Username\" name=\"userame\""
                                        + "id=\"username\" class=\"form-control\">"
                                        + "</div>"
                                        + "<div class=\"form-group\">"
                                        + "<span class=\"glyphicon glyphicon-remove-sign\" aria-hidden=\"true\"></span>"
                                        + "<input type=\"password\" placeholder=\"Password\" name=\"password\""
                                        + "id=\"password\" class=\"form-control\">"
                                        + "</div>"
                                        + "<button type=\"submit\" class=\"btn btn-success\" id=\"signInBtn\">Sign"
                                        + "In</button>"
                                        + "<button type=\"button\" class=\"btn btn-danger\" id=\"signUpBtn\">Sign"
                                        + "Up</button>" + "</form></ul>";
                }
                
                
                request.setAttribute("loginForm", loggedDropDown);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
