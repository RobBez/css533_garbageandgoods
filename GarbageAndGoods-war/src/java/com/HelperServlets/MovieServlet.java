/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.HelperServlets;

import com.ejb.Movie;
import com.ejb.MovieFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author robbez
 */
public class MovieServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @EJB
    private MovieFacadeLocal mf;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        boolean signedIn = (String) request.getSession().getAttribute("userid") != null;
        String movieID = request.getParameter("movieID");
        Movie retrievedMovie = mf.find(movieID); 
        
        if(retrievedMovie != null)
        {
            Map movie = new HashMap();
            movie.put("movieTitle", retrievedMovie.getTitle());
            movie.put("movieImgURL", retrievedMovie.getCoverImage());
            movie.put("movieDescription", retrievedMovie.getSynopsis());
            movie.put("movieReleaseDate", retrievedMovie.getReleaseDate());
            movie.put("moviePrice", retrievedMovie.getPurchasePrice());
            movie.put("movieDuration", retrievedMovie.getDuration());
            movie.put("movieRating", retrievedMovie.getReviewRating());

            String stock;
	    String stockClass;
		
            if(retrievedMovie.getInventory() == 0){
                     stock = "Out of Stock";
                     stockClass = "product-no-stock";
            } else {
                     stock = "In Stock";
                     stockClass = "product-stock";
            }
		
            String purchaseButtonStatus = "";

            if(!signedIn || retrievedMovie.getInventory() == 0){
                purchaseButtonStatus = "disabled=\"disabled\"";
            }
            
            movie.put("movieStock", stock);
            movie.put("movieStockClass", stockClass);
            movie.put("moviePurchaseButtonStatus", purchaseButtonStatus);
            
            request.setAttribute("movie", movie);
        } else
        {
              try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet MovieServlet</title>");            
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Movie with ID: " + movieID + " doesn't exist</h1>");
                out.println("</body>");
                out.println("</html>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
