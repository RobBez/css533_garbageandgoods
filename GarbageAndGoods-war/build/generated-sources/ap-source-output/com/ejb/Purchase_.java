package com.ejb;

import com.ejb.Card;
import com.ejb.Customer;
import com.ejb.Movie;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-18T17:00:29")
@StaticMetamodel(Purchase.class)
public class Purchase_ { 

    public static volatile SingularAttribute<Purchase, Date> purchaseDate;
    public static volatile SingularAttribute<Purchase, Double> totalPrice;
    public static volatile SingularAttribute<Purchase, Integer> purchaseID;
    public static volatile SingularAttribute<Purchase, Card> cardID;
    public static volatile SingularAttribute<Purchase, Customer> customerID;
    public static volatile SingularAttribute<Purchase, Movie> movieID;

}