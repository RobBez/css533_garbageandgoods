package com.ejb;

import com.ejb.Purchase;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-18T17:00:29")
@StaticMetamodel(Movie.class)
public class Movie_ { 

    public static volatile SingularAttribute<Movie, Date> duration;
    public static volatile SingularAttribute<Movie, Date> releaseDate;
    public static volatile SingularAttribute<Movie, String> coverImage;
    public static volatile SingularAttribute<Movie, String> movieID;
    public static volatile SingularAttribute<Movie, Double> purchasePrice;
    public static volatile SingularAttribute<Movie, String> synopsis;
    public static volatile SingularAttribute<Movie, String> title;
    public static volatile SingularAttribute<Movie, Integer> inventory;
    public static volatile CollectionAttribute<Movie, Purchase> purchaseCollection;
    public static volatile SingularAttribute<Movie, Double> reviewRating;
    public static volatile SingularAttribute<Movie, String> movieLink;

}