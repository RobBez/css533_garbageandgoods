package com.ejb;

import com.ejb.Card;
import com.ejb.Purchase;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-18T17:00:29")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, String> customerEmail;
    public static volatile SingularAttribute<Customer, String> customerPassword;
    public static volatile SingularAttribute<Customer, String> customerID;
    public static volatile SingularAttribute<Customer, Date> dateOfBirth;
    public static volatile CollectionAttribute<Customer, Purchase> purchaseCollection;
    public static volatile CollectionAttribute<Customer, Card> cardCollection;
    public static volatile SingularAttribute<Customer, String> customerName;

}