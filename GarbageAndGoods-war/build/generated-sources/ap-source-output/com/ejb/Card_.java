package com.ejb;

import com.ejb.Customer;
import com.ejb.Purchase;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-18T17:00:29")
@StaticMetamodel(Card.class)
public class Card_ { 

    public static volatile SingularAttribute<Card, String> cardCVS;
    public static volatile CollectionAttribute<Card, Customer> customerCollection;
    public static volatile SingularAttribute<Card, String> cardID;
    public static volatile SingularAttribute<Card, Date> cardExpire;
    public static volatile SingularAttribute<Card, String> cardHolder;
    public static volatile CollectionAttribute<Card, Purchase> purchaseCollection;

}